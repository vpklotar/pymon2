from PyMon.Backend.DynLoad import DynLoad
import GlobalConfig


class DatabaseManager:
    Databases = None  # List of all databases initialized

    def __init__(self):
        self.Config = GlobalConfig.GetValue('Config')
        self.Databases = list()
        self.Logger = GlobalConfig.GetLogger('Database Manager')
        self.DynLoad = DynLoad()
        self.LoadDatabases()

    # Go through all databases in defined in the Config
    # and initialize them acordingly
    def LoadDatabases(self):
        # self.Logger.Log("Trying to initialize DataBases", self.Logger.LogLevel.INFO)
        if "databases" in self.Config.Data:
            for database in self.Config.Data["databases"]:
                if "type" in database:
                    self.LoadDatabase(database)
                else:
                    self.Logger.error("Database has no 'type' and will not be loaded")

    def LoadDatabase(self, database):
        type = database["type"]
        db = self.DynLoad.LoadDatabase(type)
        self.Databases.append(db(database))

    def GetAllDatabases(self):
        return self.Databases

    def GetDatabaseByName(self, Name):
        databases = self.GetAllDatabases()
        for db in databases:
            if db.Name == Name:
                return db
        return None
