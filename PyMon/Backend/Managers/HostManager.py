import GlobalConfig
from PyMon.Backend.Components.Host import Host


class HostManager:
    Hosts = None  # List of all hosts

    def __init__(self):
        self.Hosts = list()
        self.DatabaseManager = GlobalConfig.GetValue('DatabaseManager')
        self.Logger = GlobalConfig.GetLogger('Host Manager')
        self.Logger.info("Initializing Host Manager")
        self.Logger.info("Initilization hosts")
        self.InitHosts()
        self.Logger.info("Initilization of Host Manager done")

    def InitHosts(self):
        for db in self.DatabaseManager.GetAllDatabases():
            for host in db.GetAllHosts():
                self.StartHost(host, db)

    def StartHost(self, config, Database):
        if "Name" in config:
            self.Logger.info("Initializing host %s" % config['Name'])
            self.Hosts.append(Host(config, self.DatabaseManager))
        else:
            self.Logger.error("Host has no Name defined, skipping")

    def GetAllHosts(self):
        return self.Hosts

    def GetHostById(self, Id):
        for host in self.GetAllHosts():
            if host.ID == str(Id):
                return host
        return None
