import threading
import GlobalConfig
from PyMon.Backend.BaseClasses.Monitor import Monitor
from PyMon.Backend.DynLoad import DynLoad


class MonitorManager:
    Monitors = None  # Dictionary of all monitors this instance have

    def __init__(self, Host, DatabaseManager):
        self.Host = Host
        self.Monitors = {}
        self.DatabaseManager = DatabaseManager
        self.DynLoad = DynLoad()
        self.Logger = GlobalConfig.GetLogger("Monitor Manager %s" % self.Host.Name)
        self.Logger.info("Initializing monitors for host '%s'" % self.Host.Name)
        self.InitializeMonitors()
        self.Logger.info("Initializing done")

    def InitializeMonitors(self):
        for db in self.DatabaseManager.GetAllDatabases():
            for monitor in db.SearchMonitor({"HostID": self.Host.ID}):
                if "Type" in monitor:
                    ref = self.DynLoad.LoadMonitor(monitor["Type"])
                    mon = ref(monitor, self.Host, db, self)
                    self.StartMonitor(mon)
                else:
                    self.Logger.error("Monitor does not define a Type")

    def StartMonitor(self, Monitor):
        self.Logger.info("Starting monitor %s" % Monitor.Name)
        t = threading._start_new_thread(Monitor.Run, ())
        self.Monitors[Monitor] = t
        self.Update()

    def GetAllMonitors(self):
        return self.Monitors

    def CreateMonitor(self, data):
        # First of all, make sure all variables needed are passed in
        if "Type" not in data:
            return {"result": "error", "error": "No Type set"}
        if "DatabaseID" not in data:
            return {"result": "error", "error": "No DatabaseID set"}
        else:
            db = self.DatabaseManager.GetDatabaseByName(data['DatabaseID'])
            # Check that the DB specified actually exist
            if db is None:
                return {"result": "error", "error": "A database with that ID does not exist"}
        # Try loading the Type specified
        try:
            ref = self.DynLoad.LoadMonitor(data['Type'])
        except ImportError:
            return {"result": "error", "error": "A monitor of type %s does not exist" % data['Type']}
        # Get the requiered frield from the Monitor
        RequieredFields = ref.GetRequieredFields()
        # Make sure all requiered fields are indeed passed in to the monitor
        for field in RequieredFields:
            if field not in data:
                return {"result": "error", "error": "No %s set" % field}
        # Insert the new monitor into the DB and the the inserted ID
        _id = db.InsertMonitor(data)
        # Set the inserted ID into the data to be passed into the monitor
        # This makes it so it has ALL the data it would if it would have been
        # initiated from the InitializeMonitors method
        data['_id'] = str(_id)
        # Initiate the monitor with the data given, the host instance, what LogLevel to use and the DB
        mon = ref(data, self.Host, self.LogLevel, db, self)
        # Start the monitor in its own thread
        self.StartMonitor(mon)
        # Get the data from the DB and return it as a return for the REST api
        inserted = db.SearchMonitor({"_id": _id})
        return {
            "result": "ok",
            "data": inserted
        }

    # This function will go through all monitors
    # And get the worst status on ANY of them
    def Update(self):
        worst = Monitor.MonitorStatus.UNKNOWN.value
        for monitor in self.GetAllMonitors():
            if monitor.Status > worst:
                worst = monitor.Status
        self.Status = worst

    def GetStatus(self):
        return self._status

    # Set the status of this MonitorManager and the status of the
    # host assigned to it and then print a log-message
    def SetStatus(self, value):
        if(value != self.Status):
            self._status = value
            STATUS = GlobalConfig.GetValue('LoggingLevels')['STATUS']
            self.Host.Status = self.Status
            if(self.Host.Status == Monitor.MonitorStatus.OK.value):
                color = '\033[92m'
            else:
                color = '\033[91m'
            self.Host.Logger.log(STATUS, color + "Status changed to %s" % Monitor.MonitorStatus(self.Host.Status).name)

    _status = Monitor.MonitorStatus.UNKNOWN
    Status = property(GetStatus, SetStatus)
