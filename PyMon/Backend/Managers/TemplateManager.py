import GlobalConfig
from PyMon.Backend.BaseClasses.Properties import Properties


class TemplateManager:
    Name = None  # The name
    Templates = None  # List of all templates loaded

    def __init__(self):
        self.Templates = list()
        self.Logger = GlobalConfig.GetLogger('Template Manager')
        self.Logger.debug('Loading data')
        self.LoadData()

    def LoadData(self):
        self.DatabaseManager = GlobalConfig.GetValue('DatabaseManager')
        for db in self.DatabaseManager.GetAllDatabases():
            for template in db.GetAllTemplates():
                inst = self.InitilizeTemplate(template)
                if inst is not None:
                    self.Templates.append(inst)

    def InitilizeTemplate(self, data):
        if "Name" in data:
            self.Logger.debug("Initilizing template %s" % data['Name'])
            inst = Template(data)
            return inst
        else:
            self.Logger.error("Template is missing Name-key, skipping")

    def CreateNewTemplate(self, data):
        if "Name" not in data:
            return {"result": "error", "error": "A Name key does not exist"}
        if "Logo" not in data:
            data['Logo'] = ""
        if "Monitors" not in data:
            return {"result": "error", "error": "A Monitors key does not exist"}
        if "Database" not in data:
            return {"result": "error", "error": "A Database key does not exist"}
        template = Template(data)
        template.Save()
        self.Templates.append(template)

    def GetAllTemplates(self):
        return self.Templates


class Template:

    def __init__(self, data):
        self.Properties = Properties()
        self.Properties.LoadJson(data)
        self.DatabaseManager = GlobalConfig.GetValue('DatabaseManager')

    def Save(self):
        DB = self.DatabaseManager.GetDatabaseByName(self.Properties.GetProperty('Database'))
        DB.InsertTemplate(self)
