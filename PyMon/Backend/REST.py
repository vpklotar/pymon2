from flask import Flask, jsonify, request
import GlobalConfig
import _thread
from PyMon.Backend.BaseClasses.Monitor import Monitor


class REST:
    app = Flask(__name__)

    def __init__(self, main):
        global REST
        REST = self
        self.main = main
        self.HostManager = main.HostManager
        self.DatabaseManager = main.DatabaseManager

    @app.route('/hosts', methods=['GET'])
    def GetHosts():
        hosts = REST.HostManager.GetAllHosts()
        result = []
        for host in hosts:
            props = dict(host.Properties.GetAllProperties())
            props['StatusName'] = Monitor.MonitorStatus(host.Status).name
            result.append(props)
        return jsonify(result)

    @app.route('/monitors', methods=['GET'])
    def GetMonitors():
        hosts = REST.HostManager.GetAllHosts()
        result = []
        for host in hosts:
            for monitor in host.MonitorManager.GetAllMonitors():
                props = dict(monitor.Properties.GetAllProperties())
                props['StatusName'] = Monitor.MonitorStatus(monitor.Status).name
                result.append(props)
        return jsonify(result)

    @app.route('/monitors', methods=['POST'])
    def AddMonitor():
        data = request.get_json(force=True)
        if "HostID" not in data:
            return jsonify({"result": "error", "error": "No HostID set"})
        host = REST.HostManager.GetHostById(str(data['HostID']))
        if host is None:
            return jsonify({"result": "error", "error": "A host with that ID does not exist"})
        else:
            return jsonify(host.MonitorManager.CreateMonitor(data))

    @app.route('/template', methods=['POST'])
    def AddTemplate():
        TemplateManager = GlobalConfig.GetValue('TemplateManager')
        data = request.get_json(force=True)
        return jsonify(TemplateManager.CreateNewTemplate(data))

    @app.route('/template', methods=['GET'])
    def GetAllTemplates():
        TemplateManager = GlobalConfig.GetValue('TemplateManager')
        result = list()
        for template in TemplateManager.GetAllTemplates():
            result.append(template.Properties.GetAllProperties())
        return jsonify(result)

    def RunRestAPI(self, newThread=True, debug=False):
        handler = GlobalConfig.GetValue('LogHandler')
        self.app.logger.addHandler(handler)
        self.app.logger.info('TEST')
        self.app.config['JSON_SORT_KEYS'] = False
        if(newThread):
            _thread.start_new_thread(self.RunRestAPI, (False, debug))
        elif(not newThread):
            self.app.run(host='0.0.0.0')
        elif(debug):
            self.app.run(debug=debug, host='0.0.0.0')

    @app.after_request
    def after_request(response):
        response.headers.add('Access-Control-Allow-Origin', '*')
        response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
        response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
        return response
