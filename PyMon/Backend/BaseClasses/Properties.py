import json


class Properties:
    Properties = None  # The dictionary that holds ALL properties

    def __init__(self, Properties=None):
        self.Properties = {}
        if(Properties is not None):
            self.LoadProperties(Properties)

    #  Will incorperate all properties from a given Properties-class into this
    def LoadProperties(self, Properties, Override=False):
        AllProps = Properties.GetAllProperties()
        for name in AllProps.keys():
            self.SetProperty(name, AllProps[name], Override)

    #  Will return a boolean indicating if the given value
    #  has been set or not
    def IsPropertySet(self, name):
        return name in self.Properties

    #  Will set a property on this object
    def SetProperty(self, Name, Value, Override=False):
        if(self.IsPropertySet(Name)):
            if(Override):
                self.Properties[Name] = Value
        else:
            self.Properties[Name] = Value

    def GetProperty(self, name, default=None):
        if(name in self.GetAllProperties().keys()):
            return self.GetAllProperties()[name]
        else:
            return default

    #  Will loop through a dict and set all values
    def SetMutlipleProperties(self, Dict, Override=False):
        for Name in Dict:
            self.SetProperty(Name, Dict[Name], Override)

    # Return all propeties in this object
    def GetAllProperties(self):
        return self.Properties

    # Convert all values in this object o JSON
    def ToJson(self):
        return json.dumps(self.Properties)

    def LoadJson(self, json, Override=False):
        for key in json:
            self.SetProperty(key, json[key], Override)
