from enum import Enum
import GlobalConfig
from PyMon.Backend.BaseClasses.Properties import Properties


class Monitor:

    class MonitorStatus(Enum):
        UNKNOWN = 0
        OK = 1
        NOK = 2
        NOT_REACHABLE = 3
        TIMEOUT = 4

    @staticmethod
    def GetRequieredFields():
        return ["Type", "HostID"]

    RequieredFields = None  # List of RequieredFields when creating this monitor

    def __init__(self, Config, Host, Database, MonitorManager):
        self.Properties = Properties()
        self.Properties.LoadJson(Config)
        self.Database = Database
        self.Config = Config
        self.Host = Host
        self.MonitorManager = MonitorManager
        self.LoadProperties()
        self.Name = str(type(self).__name__)
        self.Logger = GlobalConfig.GetLogger("Monitor %s-%s" % (self.Host.Name, self.Name))
        self.PrintDebug()
        if not self.Properties.IsPropertySet("Status"):
            self.Status = self.MonitorStatus.UNKNOWN
        else:
            self.SetStatus(Monitor.MonitorStatus(self.Properties.GetProperty('Status')))
            if(self.Status == self.MonitorStatus.OK.value):
                color = '\033[92m'
            else:
                color = '\033[91m'
            self.Logger.debug(color + 'Restoring status %s' % self.MonitorStatus(self.Properties.GetProperty('Status')).name)
        self.Logger.debug("Initilization done")

    def PrintDebug(self):
        props = self.Properties.GetAllProperties()
        PROPERTY = GlobalConfig.GetValue('LoggingLevels')['PROPERTY']
        self.Logger.log(PROPERTY, "The following %i lines are all properties of host %s" % (len(props), self.Name))
        for key in props:
            self.Logger.log(PROPERTY, "%s: %s (Type: %s)" % (key, str(props[key]), type(props[key])))

    def Run(self):
        raise NotImplementedError()

    def LoadProperties(self):
        pass

    def SetStatus(self, value):
        if(isinstance(value, self.MonitorStatus)):
            value = value.value

        if(value != self.Status):
            self.Properties.SetProperty("Status", value, True)
            self.PrintStatusUpdate()
            self.Database.InsertMonitorStatusUpdate(self)
            self.MonitorManager.Update()
            self.Save()

    def PrintStatusUpdate(self):
        STATUS = GlobalConfig.GetValue('LoggingLevels')['STATUS']
        if(self.Status == self.MonitorStatus.OK.value):
            color = '\033[92m'
        else:
            color = '\033[91m'
        self.Logger.log(STATUS, color + "Status changed to %s" % Monitor.MonitorStatus(self.Status).name)

    def GetStatus(self):
        return self.Properties.GetProperty("Status")

    # The status of this monitor
    Status = property(GetStatus, SetStatus)

    # Save current properties in database
    def Save(self):
        self.Database.UpdateMonitor(self)
