from PyMon.Backend.BaseClasses.Properties import Properties
import GlobalConfig
from PyMon.Backend.Managers.MonitorManager import MonitorManager
from PyMon.Backend.BaseClasses.Monitor import Monitor


class Host:
    # All properties are defined below
    Properties = None  # The holding properties object
    Logger = None      # The Logger used to log
    Templates = None   # List of templates assigned to this host

    def __init__(self, Data, DatabaseManager):
        self.Data = Data
        self.InitProperties()
        self.Logger = GlobalConfig.GetLogger("HOST %s" % Data['Name'])
        self.PrintDebug()
        self.Logger.info("Initilization of host '" + self.Name + "' done")
        self.MonitorManager = MonitorManager(self, DatabaseManager)

    def PrintDebug(self):
        props = self.Properties.GetAllProperties()
        PROPERTY = GlobalConfig.GetValue('LoggingLevels')['PROPERTY']
        self.Logger.log(PROPERTY, "The following %i lines are all properties of host %s" % (len(props), self.Name))
        for key in props:
            self.Logger.log(PROPERTY, "%s: %s (Type: %s)" % (key, str(props[key]), type(props[key])))

    def InitProperties(self):
        self.Properties = Properties()
        self.LoadValues()

    def LoadValues(self):
        self.Properties.LoadJson(self.Data)

    def SetAddress(self, address):
        self.Properties.SetProperty("Address", address, True)

    def GetAddress(self):
        return self.Properties.GetProperty("Address")

    # The IP/DNS address to a given Host
    Address = property(GetAddress, SetAddress)

    def SetName(self, name):
        self.Properties.SetProperty("Name", name, True)

    def GetName(self):
        return self.Properties.GetProperty("Name")

    # The Name given to a host
    Name = property(GetName, SetName)

    def SetStatus(self, status):
        self.Properties.SetProperty("Status", status, True)
        self.Properties.SetProperty("StatusName", Monitor.MonitorStatus(status).name, True)

    def GetStatus(self):
        Status = self.Properties.GetProperty("Status")
        if Status is not None:
            return Status
        else:
            return Monitor.MonitorStatus.UNKNOWN.value

    # Current status of the host (worst status of any plugin)
    Status = property(GetStatus, SetStatus)

    def SetGroup(self, status):
        self.Properties.SetProperty("Group", status, True)

    def GetGroup(self):
        group = self.Properties.GetProperty("Group")
        if group is not None:
            return group
        else:
            return ""

    # Current status of the host (worst status of any plugin)
    Group = property(GetGroup, SetGroup)

    def GetStatus(self):
        status = self.Properties.GetProperty("Status")
        if status is not None:
            return status
        else:
            return Monitor.MonitorStatus.UNKNOWN.value

    def SetStatus(self, status):
        self.Properties.SetProperty("Status", status, True)

    Status = property(GetStatus, SetStatus)

    def GetID(self):
        return str(self.Properties.GetProperty('_id'))

    ID = property(GetID)
