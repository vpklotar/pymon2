from enum import Enum


class HostStatus(Enum):
    Unknown = 0
    Ok = 1
    Warning = 2
    Error = 3
