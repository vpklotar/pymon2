import GlobalConfig
import json


class Config:

    def __init__(self, path):
        string_data = self.ReadConfigFile(path)
        self.Data = json.loads(string_data)
        self.Logger = GlobalConfig.GetLogger('Config')
        self.Logger.debug("Loaded JSON")
        self.Logger.info("Config loaded completely")

    def ReadConfigFile(self, path):
        try:
            with open(path, 'r') as f:
                string_data = f.read()
        except FileNotFoundError:
            pass
        return string_data
