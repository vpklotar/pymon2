# This class handles all dynamic loading of all classes
import GlobalConfig


class DynLoad:

    def __init__(self):
        self.Logger = GlobalConfig.GetLogger('Dynamic Loader')

    # Load a Monitor
    def LoadMonitor(self, name):
        ref = self.my_import("PyMon.Monitor." + name)
        return ref

    # Load a Reciver
    def LoadReciver(self, name):
        ref = self.my_import("PyMon.Reciver." + name)
        return ref

    # Load a Transformer
    def LoadTransformer(self, name):
        ref = self.my_import("PyMon.Transformer." + name)
        return ref

    # Load a Database
    def LoadDatabase(self, name):
        ref = self.my_import("PyMon.DatabaseProvider." + name)
        return ref

    # Dynamicly load a module given a path
    # Return: A reference that can be instanciated
    def my_import(self, path):
        components = path.split('.')
        self.Logger.debug("Trying to load " + path)
        mod = __import__(path)
        for comp in components[1:]:
            mod = getattr(mod, comp)
        ref = getattr(mod, components[-1])
        self.Logger.debug("Load of '" + path + "' completed")
        return ref
