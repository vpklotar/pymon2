from PyMon.Backend.BaseClasses.Monitor import Monitor
from time import sleep
import subprocess


class Ping(Monitor):
    Host = None  # The host this monitor belongs to

    def LoadProperties(self):
        self.Timeout = int(self.Properties.GetProperty("Timeout", "1"))
        self.Interval = int(self.Properties.GetProperty("Interval", "1"))

    def Run(self):
        while True:
            process = subprocess.Popen(["ping", "-c1", self.Host.Address], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            try:
                # Run the command above and set correct status
                process.communicate(timeout=self.Timeout)
                if (process.returncode == 0):
                    self.Status = Monitor.MonitorStatus.OK
                else:
                    self.Status = Monitor.MonitorStatus.NOT_REACHABLE
            except subprocess.TimeoutExpired:
                self.Status = Monitor.MonitorStatus.TIMEOUT
            sleep(self.Interval)
