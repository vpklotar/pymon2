import GlobalConfig
from pymongo import MongoClient
from bson.objectid import ObjectId
import time


class MongoDB:

    def __init__(self, json):
        self.json = json
        self.Logger = GlobalConfig.GetLogger("MongoDB %s" % json['name'])
        self.Logger.info("initializing MongoDB " + json["name"])
        self.CopyData()
        self.InitDatabase()

    # Copy all variables/keys needed into variables
    def CopyData(self):
        self.Name = self.GetJsonData("name")
        self.Type = self.GetJsonData("type")
        self.Host = self.GetJsonData("host")
        self.Port = self.GetJsonData("port", 27017)
        self.Username = self.GetJsonData("username")
        self.Password = self.GetJsonData("password")
        self.DBName = self.GetJsonData("dbname", "pymon")
        self.HostsCollection = self.GetJsonData("hosts_collection", "hosts")
        self.MonitorCollection = self.GetJsonData("monitor_collection", "monitors")
        self.TemplateCollection = self.GetJsonData("template_collection", "templates")

    # Try to get a value from the json-data
    # If the key does not exist it print a
    # error to the user
    def GetJsonData(self, name, default=None):
        if name in self.json:
            return self.json[name]
        else:
            if default is None:
                self.Logger.error("Key '%s' not set!" % name)
            return default

    def InitDatabase(self):
        self.Logger.info("Starting initilization of DB-client")
        self.Client = MongoClient(self.Host, self.Port)
        self.Logger.debug("Client constructed")
        self.DB = self.Client[self.DBName]
        self.Logger.debug("DB Selected")
        self.Logger.info("initilization of DB-Client done")

    def GetAllHosts(self):
        collection = self.DB[self.HostsCollection]
        result = []
        for doc in collection.find({}):
            doc['_id'] = str(doc['_id'])
            doc['_dbname'] = self.Name
            result.append(doc)
        return result

    def SearchMonitor(self, pattern):
        collection = self.DB[self.MonitorCollection]
        result = []
        for doc in collection.find(pattern):
            doc['_id'] = str(doc['_id'])
            doc['_dbname'] = self.Name
            result.append(doc)
        return result

    def InsertMonitorStatusUpdate(self, monitor):
        collection = self.DB['status']
        result = collection.insert_one({
            "Host": monitor.Host.Properties.GetProperty("_id"),
            "Monitor": monitor.Properties.GetProperty("_id"),
            "Status": monitor.Status,
            "Time": self.GetCurrentTimeMills()
        })
        if(result is None or result.inserted_id is None):
            self.Logger.error("Failed to insert status update for monitor %s on host %s" % (monitor.Name, monitor.Host.Name))

    def InsertMonitor(self, properties):
        collection = self.DB[self.MonitorCollection]
        result = collection.insert_one(properties)
        if(result is None or result.inserted_id is None):
            self.Logger.error("Failed to insert new monitor")
        return result.inserted_id

    def UpdateMonitor(self, Monitor):
        collection = self.DB[self.MonitorCollection]
        props = dict(Monitor.Properties.GetAllProperties())
        oid = props['_id']
        del props['_id']
        result = collection.update_one({
            '_id': ObjectId(oid)
        }, {
            '$set': props
        })

        if(result is None or result.modified_count <= 0):
            self.Logger.error("Failed to update monitor %s on host %s" % (Monitor.Name, Monitor.Host.Name))
            print(props)
            print(result.raw_result)

    def InsertTemplate(self, template):
        collection = self.DB[self.TemplateCollection]
        result = collection.insert_one(template.Properties.GetAllProperties())
        if(result is None or result.inserted_id is None):
            self.Logger.error("Failed to insert new template")
        return result.inserted_id

    def GetAllTemplates(self):
        return self.SearchTemplate({})

    def SearchTemplate(self, pattern):
        collection = self.DB[self.TemplateCollection]
        result = []
        for doc in collection.find(pattern):
            doc['_id'] = str(doc['_id'])
            doc['_dbname'] = self.Name
            result.append(doc)
        return result

    def GetCurrentTimeMills(self):
        return int(round(time.time() * 1000))
