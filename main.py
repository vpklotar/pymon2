#!/usr/bin/env python3
import time

from Backend.Config import Config
from Backend.Logging import Logging
import logging
import os
from Backend.ComponentLoader import ComponentLoader


class Main:
    logger: logging.Logger

    def __init__(self):
        self.logger = logging.getLogger()
        Config()  # Initialize the Config singleton so that it is ready when accessed
        Logging.init_logging()  # Initialize the Logging singleton
        self.logger.info('Starting PyMon2')
        self.perform_sanity_check()
        ComponentLoader().load_components_from_config()

    @staticmethod
    def perform_sanity_check():
        # Check that the objects directory exists
        objects_directory = Config().get(path='paths.objects_directory', default='objects')
        if not os.path.isdir(objects_directory):
            logging.error(F"Objects directory {objects_directory} does not exist!")

        # Check that the modules directory exists
        objects_directory = Config().get(path='paths.modules_directory', default='objects')
        if not os.path.isdir(objects_directory):
            logging.error(F"Modules directory {objects_directory} does not exist!")

        if Config().exist(path='datastore.configuration.driver'):
            logging.error(F"No configuration datastore configured!")


async def start_application():
    Main()

if __name__ == "__main__":
    Main()
    while True:
        time.sleep(1)
    #logging.error("PyMon2 exited unexpectedly!")
