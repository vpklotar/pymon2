from threading import Thread

from Backend.Component import Component
from components.DBManager import DBManager
from components.ServiceManager import ServiceManager
from Backend.Module import Module
import logging
from Backend.Singleton import Singleton
from Backend.StatusObject import StatusObject
from Backend.ORM.Base import HistoryModel, ServiceModel
import datetime


class StatusMonitor(Singleton, Component):
    _current_status = {}

    def run(self):
        pass

    @staticmethod
    def version() -> str:
        return "1.0"

    def __init__(self):
        self.logger = logging.getLogger('StatusMonitor')

    def connect_signals(self):
        """
        Connect to all signals to listen to
        :return: None
        """
        ServiceManager().on_service_added.connect(self.service_added)

    def service_added(self, module: Module):
        """
        When a module has been loaded, start listening to status changes from that module
        :param module: Newly added module
        :return: None
        """
        self._current_status[module] = module.status.result
        module.on_status_update.connect(self.status_updated)

    def status_updated(self, module: Module, status: StatusObject):
        """
        When a status update on a service occurs, update the database
        :param module: The service that changed state
        :param status: The new status
        :return: None
        """
        # Only update "history" if the result itself (and not only the status string) has changed.
        if module.status.result != self._current_status[module]:
            hm = HistoryModel()
            hm.status_result = int(status.result)
            hm.status_string = status.status_string
            hm.datetime = status.updated_at
            hm.host_id = module.host.id
            hm.service_id = module.id
            hm.save()
            self._current_status[module] = module.status.result
