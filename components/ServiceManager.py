import time
from datetime import datetime

from typing import List

from Backend.Blueprint import Blueprint
from Backend.Component import Component
from Backend.Host import Host
from Backend.ORM.Base import ServiceModel
from components.BlueprintManager import BlueprintManager
from components.ModuleManager import ModuleManager
from Backend.Signal import Signal
from Backend.Singleton import Singleton
import logging
from threading import Lock
from threading import Thread


class ServiceManager(Singleton, Component):
    _loaded_services: List = list()
    on_service_added: Signal = Signal(object)
    _thread_lock: Lock = Lock()
    _check_thread: Thread = None

    def run(self):
        self._start_service_threads()
        # Listen to events from already instantiated blueprints
        for blueprint in BlueprintManager().blueprints.values():
            blueprint.on_host_added.connect(self._on_host_added_to_blueprint)

    @staticmethod
    def version():
        return "1.0"

    @property
    def services(self) -> List:
        return self._loaded_services

    def __init__(self):
        super().__init__()
        self.logger = logging.getLogger('ServiceManager')

    def _on_host_added_to_blueprint(self, blueprint: Blueprint, host: Host):
        """
        A new host was added to a already existing blueprint. Make sure to apply all services from
        blueprint to the new host
        :param blueprint: The blueprint
        :param host: The host that was added to the blueprint
        :return:
        """
        self._load_service_from_blueprint_for_host(blueprint, host)

    def _load_services_from_blueprint(self, blueprint: Blueprint):
        """
        When a new blueprint has been loaded. Load all the services associated with that blueprint
        :param blueprint: The blueprint that has been loaded
        :return:
        """
        blueprint.on_host_added.connect(self._on_host_added_to_blueprint)
        for host in blueprint.hosts:
            self._load_service_from_blueprint_for_host(blueprint, host)

    def _load_service_from_blueprint_for_host(self, blueprint: Blueprint, host: Host):
        """
        Loop through all services in a blueprint and instanciate them for a new host
        :param blueprint: The blueprint to be applied to a host
        :param host: The host
        :return:
        """
        for service in blueprint.raw_services:
            self._create_service_thread(host=host, service_model=service, blueprint=blueprint)

    def _create_service_thread(self, host: Host, service_model: ServiceModel, blueprint: Blueprint):
        """
        Load a given service, attach it to a host and blueprint and add it to the list if checks to be performed
        :param host: The host to attach the service to
        :param service_model: The service that is to be instantiated
        :param blueprint: The blueprint this service is derived from
        :return: None
        """
        with self._thread_lock:
            module = ModuleManager().get_instance(host=host, module_name=service_model.module,
                                                  service_model=service_model, blueprint=blueprint)
            if module:
                self._loaded_services.append(module)
                self.logger.info(F"Loaded service '{module.module}' for host '{module.host}' successfully.")
                blueprint.add_service(module)
                host.attach_to_service(module)
                self.on_service_added.emit(module)

    def _start_service_threads(self):
        """
        Start a separate thread to handle start of services on time
        :return:
        """
        with self._thread_lock:
            if self._check_thread is None:
                self._check_thread = Thread(target=self._run_check_thread)
                self._check_thread.daemon = True
                self._check_thread.start()

    def _run_check_thread(self):
        """
        The thread that makes sure that new checks are fired off when needed
        :return:
        """
        while True:
            with self._thread_lock:
                current_time = datetime.utcnow()
                for service in self._loaded_services:
                    if (current_time - service.status.updated_at).total_seconds() >= service.check_interval:
                        if not service.running and service.do_active_checks:
                            # Fire off a new check
                            try:
                                t = Thread(target=service.execute_check)
                                t.daemon = True
                                t.start()
                            except Exception as e:
                                self.logger.error(F"Error while running {service}:")
                                self.logger.error(e)
            time.sleep(1)

    def stop_service(self, service):
        """
        Will remove a service from the "run list". The service given will not be run again after it
        has completed the current run (if currently running)
        :param service: The service to remove
        :return:
        """
        with self._thread_lock:
            # Delete the status attached to the service
            self._loaded_services.remove(service)
            self.logger.info(F"Successfully removed service '{service.module}' for host '{service.host}'")

    def connect_signals(self):
        BlueprintManager().on_blueprint_loaded.connect(self._load_services_from_blueprint)
