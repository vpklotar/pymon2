from Backend.Component import Component
from Backend.ORM.Base import HostModel
from Backend.Signal import Signal
from Backend.Host import Host
from Backend.Exceptions import MissingArgumentException
import logging
from Backend.Singleton import Singleton


class HostManager(Singleton, Component):
    _hosts: dict = {}
    on_host_added: Signal = Signal(Host)

    @property
    def hosts(self):
        return self._hosts

    def connect_signals(self):
        pass

    @staticmethod
    def version():
        return "1.0"

    def __init__(self):
        super().__init__()
        self.logger = logging.getLogger('HostManager')

    def run(self):
        """Load a host given its json_object"""
        hosts = HostModel.all()
        for host in hosts:
            self._load_host_from_host_model(host)

    def _load_host_from_host_model(self, hostmodel: HostModel):
        try:
            new_host = Host(hostmodel)
            self._hosts[new_host.id] = new_host
            self.logger.info(F"Loaded host '{new_host.display_name}' successfully.")
            self.on_host_added.emit(new_host)
        except MissingArgumentException as e:
            self.logger.error(e)

    def create_host(self, display_name: str, address: str):
        """
        Create a new host given the needed data
        :param display_name: The name of the host
        :param address: The address to the host
        :return:
        """
        # Create the new host in the database
        hm = HostModel()
        hm.display_name = display_name
        hm.address = address
        hm.save()
        self.logger.info(F"Created new host '{hm.display_name}', id: {hm.id}")
        # Load the new host
        self._load_host_from_host_model(hm)

    def delete_host(self, host: Host):
        self._hosts.pop(host.id)
        host.host_model.delete()
        self.logger.info(F"Removed host '{host.display_name}' with id '{host.id}'")

    def get_host_by_id(self, id: int) -> Host:
        """
        Get a loaded host by ID
        :param id: The ID of the host
        :return: A Host object
        """
        for host in self._hosts.values():
            if host.id == id:
                return host