from Backend.Component import Component
from Backend.Config import Config
from Backend.Host import Host
from Backend.Module import Module
import logging
import os
from Backend.Singleton import Singleton


class ModuleManager(Singleton, Component):
    _modules = {}

    @property
    def modules(self):
        return self._modules

    def connect_signals(self):
        pass

    def run(self):
        pass

    @staticmethod
    def version() -> str:
        return "1.0"

    def _import_module(self, module_name: str):
        """Import the specified python module"""
        if module_name not in self._modules:
            modules_path = Config().get(path='paths.modules_directory', default='modules')
            module_path = os.path.join(modules_path, F"{module_name}.py")
            if os.path.isfile(module_path):
                mod = __import__(F"{modules_path}.{module_name}", fromlist=[module_name])
                self._modules[module_name] = mod
            else:
                raise FileNotFoundError(F"Module {module_name} not found. Make sure the file '{module_path}' exists.")
        else:
            logging.debug(F"Trying to load module {module_name} when it is already loaded.")

    def get_instance(self, host: Host, module_name: str, service_model, blueprint) -> Module:
        """Load and create instance of the given module"""
        try:
            self._import_module(module_name)
            module = self._modules[module_name]  # Load the module
            cls = getattr(module, module_name)  # Get the class from the loaded module
            logging.getLogger('ModuleManager').debug(F"About to instantiate module '{module_name}' "
                                                     F"version {cls.version()}")
            return cls(host, service_model, blueprint)  # Instantiate the class with the given arguments
        except Exception as e:
            logging.getLogger('ModuleManager').error(F"Failed to create instance of module {module_name}. Error:")
            logging.getLogger('ModuleManager').error(e)

