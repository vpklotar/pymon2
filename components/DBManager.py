from orator import DatabaseManager, Model

from Backend.Component import Component
from Backend.Config import Config
from Backend.Singleton import Singleton
import logging


class DBManager(Singleton, Component):
    _database: DatabaseManager = None
    
    @property
    def database_manager(self):
        return self._database

    def connect_signals(self):
        pass

    def run(self):
        self._database = DatabaseManager(Config().get(path='datastore.configuration'))
        Model.set_connection_resolver(self._database)

    @staticmethod
    def version():
        return "1.0"

    def __init__(self):
        super().__init__()
        self.logger = logging.getLogger(self.__str__())
