from typing import List, Dict

from Backend.Blueprint import Blueprint
from Backend.Component import Component
from Backend.Host import Host
from Backend.ORM.Base import BlueprintModel
from Backend.Signal import Signal
from Backend.Singleton import Singleton
from components.HostManager import HostManager
import logging


class BlueprintManager(Component, Singleton):
    _blueprints: Dict[int, Blueprint] = {}
    _logger: logging.Logger = None
    on_blueprint_loaded: Signal = Signal(Blueprint)

    @property
    def blueprints(self) -> Dict[int, Blueprint]:
        return self._blueprints

    def connect_signals(self):
        HostManager().on_host_added.connect(self._host_added)

    def _host_added(self, host: Host):
        """
        Attach the host to all the blueprints it's a member of
        :param host: The host attach
        :return: None
        """
        for host_blueprint in host.host_model.blueprints:
            blueprint = self._blueprints[host_blueprint.id]
            blueprint.attach_host(host)
            self._logger.debug(F"Attached host {host} to blueprint '{blueprint.display_name}' with id: '{blueprint.id}'")

    def run(self):
        # Load all blueprints
        for raw_blueprint in BlueprintModel.all():
            self._add_blueprint(raw_blueprint)

    def _add_blueprint(self, raw_blueprint) -> Blueprint:
        blueprint = Blueprint(raw_blueprint)
        self._blueprints[raw_blueprint.id] = blueprint
        self.on_blueprint_loaded.emit(blueprint)
        self._logger.debug(F"Loaded blueprint '{blueprint.display_name}' with id: '{blueprint.id}'")
        return blueprint

    @staticmethod
    def version() -> str:
        return "1.0"
    
    def __init__(self):
        super(BlueprintManager, self).__init__()
        self._logger = logging.getLogger('BlueprintManager')

    def create_blueprint(self, display_name, description=None) -> Blueprint:
        """
        Create a new blueprint
        :param display_name: The display_name of the blueprint
        :param description: Description of the blueprint (optional)
        :return: The Blueprint object
        """
        bm = BlueprintModel(display_name=display_name, description=description)
        bm.save()
        blueprint = self._add_blueprint(bm)
        self._logger.info(F"Created new blueprint '{blueprint.display_name}', id: '{blueprint.id}'")
        return blueprint

    def delete_blueprint(self, blueprint: Blueprint):
        """
        Delete a blueprint and all its connections
        :param blueprint: The blueprint to delete
        :return: None
        """
        blueprint.delete()
        self._blueprints.pop(blueprint.id)
        self._logger.info(F"Deleted blueprint '{blueprint.display_name}' with id: '{blueprint.id}'")

    def get_blueprint_by_id(self, id: int):
        """
        Get a loaded blueprint by ID
        :param id: The ID of the blueprint
        :return: Blueprint object or None
        """
        if id in self._blueprints.keys():
            return self._blueprints[id]
        return None
