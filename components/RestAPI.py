from flask import Flask, request
import threading
from Backend.Component import Component
from Backend.Singleton import Singleton
from components.BlueprintManager import BlueprintManager
from components.HostManager import HostManager


#########################
### RestAPI Component ###
#########################
from components.ServiceManager import ServiceManager


class RestAPI(Component, Singleton):
    _flask_thread: threading.Thread = None

    def connect_signals(self):
        pass

    def run(self):
        self._flask_thread = threading.Thread(target=app.run)
        self._flask_thread.daemon = True
        self._flask_thread.start()

    @staticmethod
    def version() -> str:
        return "1.0.0"


#######################
### Rest API Routes ###
#######################
app = Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    return {
        "status": 400,
        "error": "No such function"
    }, 404


####################
### HOST SECTION ###
####################

@app.route('/hosts', methods=['GET'])
def get_hosts():
    ret_data = {
        "hosts": list()
    }
    for host in HostManager().hosts.values():
        ret_data["hosts"].append({
            "id": host.id,
            "display_name": host.display_name
        })
    ret_data['status'] = 200
    return ret_data, 200

@app.route('/hosts', methods=['POST'])
def create_host():
    # Perform sanity check
    if "display_name" not in request.json:
        return {"status": 500, "error": "No 'display_name' given."}, 500
    if "address" not in request.json:
        return {"status": 500, "error": "No 'address' given."}, 500
    HostManager().create_host(display_name=request.json.get('display_name'), address=request.json.get('address'))
    return {"status": 200}


@app.route('/host/<int:id>', methods=['GET'])
def get_host(id: int):
    ho = HostManager().get_host_by_id(id)
    if ho:
        ret_data = {
            'id': ho.id,
            'display_name': ho.display_name,
            'blueprints': []
        }
        # Populate the "blueprints" array for the host
        for blueprint in ho.blueprints:
            blueprint_data = {
                'id': blueprint.id,
                'display_name': blueprint.display_name,
                'services': []
            }
            # Populate the "services" array for the blueprint
            for service in blueprint.services:
                blueprint_data['services'].append({
                    'id': service.id,
                    'module': service.module
                })
            ret_data['blueprints'].append(blueprint_data)
        return ret_data
    else:
        return {"status": 404, "error": "No host with that ID found."}, 404


@app.route('/host/<int:id>', methods=['PUT'])
def update_host(id: int):
    ho = HostManager().get_host_by_id(id)
    if ho:
        if 'display_name' in request.json:
            ho.display_name = request.json.get('display_name')
            return {"status": 200}, 200
        else:
            return {"status": 500, "error": "'display_name' is the only data that can be modified on a host."}, 500
    else:
        return {"status": 404, "error": "No host with that ID found."}, 404


@app.route('/host/<int:id>', methods=['DELETE'])
def delete_host(id: int):
    ho = HostManager().get_host_by_id(id)
    if ho:
        for service in ho.services:
            ServiceManager().delete_service(service)
        HostManager().delete_host(ho)
        return {"status": 200}, 200
    else:
        return {"status": 404, "error": "No host with that ID found."}, 404


########################
### SERVICES SECTION ###
########################

@app.route('/services', methods=['GET'])
def get_services():
    ret_data = {
        "services": list(),
        "status": 200
    }
    for service in ServiceManager().services:
        ret_data["services"].append({
            "id": service.id,
            "module": {
                "name": service.module,
                "version": service.version()
            },
            "check_interval": service.check_interval,
            "status": {
                "result": service.status.result,
                "string": service.status.status_string
            },
            "blueprint": {
                "id": service.blueprint.id,
                "display_name": service.blueprint.display_name
            },
            "host": {
                "id": service.host.id,
                "display_name": service.host.display_name
            }
        })
    return ret_data, 200


#########################
### BLUEPRINT SECTION ###
#########################

@app.route('/blueprints', methods=['GET'])
def get_blueprints():
    ret_data = {
        "status": 200,
        "blueprints": []
    }
    for blueprint in BlueprintManager().blueprints.values():
        blueprint_data = {
            'id': blueprint.id,
            'display_name': blueprint.display_name,
            'hosts': [],
            'services': []
        }
        for host in blueprint.hosts:
            blueprint_data['hosts'].append({
                'id': host.id,
                'display_name': host.display_name
            })
        for service in blueprint.services:
            blueprint_data['services'].append({
                'id': service.id,
                'module': service.module
            })
        ret_data['blueprints'].append(blueprint_data)
    return ret_data, 200


@app.route('/blueprints', methods=['POST'])
def create_blueprint():
    if "display_name" not in request.json:
        return {"status": 500, "error": "No 'display_name' given."}, 500
    display_name = request.json.get('display_name')
    description = request.json.get('description', None)
    blueprint = BlueprintManager().create_blueprint(display_name=display_name, description=description)
    return {"status": 200,
            "id": blueprint.id,
            "display_name": blueprint.display_name,
            "description": blueprint.description}, 200


@app.route('/blueprint/<int:id>', methods=['DELETE'])
def delete_blueprint(id: int):
    blueprint = BlueprintManager().get_blueprint_by_id(id)
    if blueprint:
        for service in blueprint.services:
            ServiceManager().stop_service(service)
        BlueprintManager().delete_blueprint(blueprint)
        return {"status": 200}, 200
    else:
        return {"status": 500, "error": "No blueprint with that ID exists."}, 500


@app.route('/blueprint/<int:id>', methods=['PUT'])
def update_blueprint(id: int):
    blueprint = BlueprintManager().get_blueprint_by_id(id)
    # Update the blueprint itself
    if "display_name" in request.json:
        blueprint.display_name = request.json.get('display_name')
    if "description" in request.json:
        blueprint.description = request.json.get('description')
    blueprint.save()
    # Update connections to the blueprint
    if "attach_hosts" in request.json:
        if type(request.json.get("attach_hosts")) is list:
            for host_id in request.json.get("attach_hosts"):
                host = HostManager().get_host_by_id(host_id)
                if host:
                    host.add_blueprint(blueprint)
                else:
                    return {"status": 500, "error": F"No host with id {host_id} exists"}, 500
        else:
            return {"status": 500, "error": "'attach_hosts' is not of type array!"}, 500
    # Update connections to the blueprint
    if "detach_hosts" in request.json:
        if type(request.json.get("detach_hosts")) is list:
            for host_id in request.json.get("detach_hosts"):
                host = HostManager().get_host_by_id(host_id)
                if host:
                    host.remove_blueprint(blueprint)
                else:
                    return {"status": 500, "error": F"No host with id {host_id} exists"}, 500
        else:
            return {"status": 500, "error": "'attach_hosts' is not of type array!"}, 500
    return {"status": 200}, 200
