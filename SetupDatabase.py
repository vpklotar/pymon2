#!/usr/bin/env python
# This file will, given the correct database inforation in 'config.json', create the database configuration.
# Things needed before running:
#   1. The database 'pymon2' created
#   2. A user with full access to the 'pymon2'-database
#   3. The user and host information configured in 'config.json'
from orator.orm import belongs_to_many

SETUP_EXAMPLE_DATA = True

import json
import logging
from orator import DatabaseManager, Schema, Model
from Backend.ORM.Base import HostModel, ServiceModel, BlueprintModel

logging.basicConfig(level=logging.INFO)

with open('config.json') as f:
    config = json.loads(f.read())

db_config = config['datastore']['configuration']
db = DatabaseManager(db_config)
Model.set_connection_resolver(db)
schema = Schema(db)

# Drop all existing tables
schema.drop_if_exists('hosts_blueprints')
schema.drop_if_exists('history')
schema.drop_if_exists('service_status')
schema.drop_if_exists('services')
schema.drop_if_exists('blueprints')
schema.drop_if_exists('hosts')

# Create 'hosts' table
with schema.create('hosts') as table:
    table.increments('id')
    table.string('display_name', 50)
    table.string('address', 50)
    table.datetime('created_at')
    table.datetime('updated_at')
logging.info("Created table 'hosts'")

# Create 'BluePrints' table
with schema.create('blueprints') as table:
    table.increments('id')
    table.string('display_name')
    table.string('description', 200).nullable()
    table.datetime('created_at')
    table.datetime('updated_at')
logging.info("Created table 'blueprints'")

# Create 'services' table
with schema.create('services') as table:
    table.increments('id')
    table.string('module', 50)
    table.integer('check_interval').default(60)
    table.json('arguments').default("{}")
    table.integer('blueprint_id').unsigned()
    table.foreign('blueprint_id').references('id').on('blueprints').on_delete('cascade')
    table.datetime('created_at')
    table.datetime('updated_at')
logging.info("Created table 'services'")

# Create 'service_status' table
with schema.create('service_status') as table:
    table.increments('id')
    table.integer('status_result').default(4)
    table.string('status_string', 100).nullable()
    table.integer('service_id').unsigned()
    table.foreign('service_id').references('id').on('services').on_delete('cascade')
    table.integer('host_id').unsigned()
    table.foreign('host_id').references('id').on('hosts').on_delete('cascade')
    table.datetime('created_at')
    table.datetime('updated_at')
logging.info("Created table 'service_status'")

# Create 'history' table
with schema.create('history') as table:
    table.increments('id')
    table.integer('status_result')
    table.datetime('datetime')
    table.string('status_string', 100).nullable()
    table.integer('host_id').unsigned()
    table.foreign('host_id').references('id').on('hosts').on_delete('cascade')
    table.integer('service_id').unsigned()
    table.foreign('service_id').references('id').on('services').on_delete('cascade')
    table.datetime('created_at')
    table.datetime('updated_at')
logging.info("Created table 'history'")

# Create 'hosts_blueprints' table to map many <-> many relationship between
# hosts and blueprints
with schema.create('hosts_blueprints') as table:
    table.increments('id')
    table.integer('host_id').unsigned()
    table.foreign('host_id').references('id').on('hosts').on_delete('cascade')
    table.integer('blueprint_id').unsigned()
    table.foreign('blueprint_id').references('id').on('blueprints').on_delete('cascade')
logging.info("Created table 'hosts_blueprints'")

###########################
### CREATE EXAMPLE DATA ###
###########################

if SETUP_EXAMPLE_DATA:
    logging.info("Inserting example data")
    hm = HostModel(display_name='Example host', address='10.0.0.2')
    hm.save()
    # Create and save example BluePrint
    bm = BlueprintModel(display_name='Example blueprint')
    hm.blueprints().save(bm)
    # Attach example services to example BluePrint
    services = [
        ServiceModel(module='Example', check_interval=10, arguments='{"running": "true"}'),
        ServiceModel(module='Ping4', check_interval=10),
    ]
    bm.services().save_many(services)
