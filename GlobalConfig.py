import colorlog


def init():
    global settingsDict
    try:
        settingsDict
    except NameError:
        settingsDict = {
            "loglevel": 7
        }


def SetValue(name, value):
    global settingsDict
    settingsDict[name] = value


def GetValue(name):
    global settingsDict
    if name in settingsDict:
        return settingsDict[name]
    return None


def GetLogger(facility):
    handler = GetValue('LogHandler')
    logger = colorlog.getLogger(facility)
    logger.addHandler(handler)
    logger.setLevel(GetValue('LogLevel'))
    return logger
