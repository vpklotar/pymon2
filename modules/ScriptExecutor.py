from Backend.Module import Module
from Backend.StatusObject import StatusObject, StatusResult
import subprocess


class Ping4(Module):

    @staticmethod
    def version() -> str:
        return "1.0"

    def execute_check(self):
        # TODO: Implement script execution logic.
        # Make sure to keep it nagios compatible to be able to reuse a lot of plugins/module/scripts
        super().execute_check()
        cmd = subprocess.Popen(['ping', '-c 1', self.host.address], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        cmd.communicate()
        print(cmd.returncode)
        self.status = StatusObject(status=StatusResult.ok)