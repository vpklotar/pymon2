"""
This file is intended to show how to create a basic PyMon2 check
"""
import time
import random

from Backend.Blueprint import Blueprint
from Backend.Module import Module
from Backend.StatusObject import StatusObject, StatusResult
from Backend.Exceptions import MissingArgumentException


class Example(Module):
    """
    The class name must match (case sensitive) the file name
    """

    def __init__(self, host, service_model, blueprint: Blueprint):
        """
        This function will run as soon as the object is instantiated using parameters from the configuration.
        :param host: The host (machine) that this object is associated with.
        :param kwargs: All the parameters given to the service object in the configuration.
        """
        super().__init__(host, service_model, blueprint)
        self.logger.debug(F"This is printed using the LOGGER. Coupled to host {self.host}")

    def perform_sanity_check(self, service_module) -> None:
        """
        This will run as soon as the object is instantiated and is intended to verify that all needed data is present.
        :return: None
        """
        if "running" not in self.arguments:
            raise MissingArgumentException(message="running is not defined!")
        if self.arguments["running"] != "true":
            raise Exception("running is not set to 'true'")

    @staticmethod
    def version() -> str:
        """
        Static method to get the service version without creating an instance of it.
        :return: The version string.
        """
        return "1.0"

    def execute_check(self) -> None:
        """
        This function will run in a separate thread than the main program and will perform the actual check.
        This function returns nothing but will update itself by settings the property self.status. By doing so
        PyMon2 will pickup on the change and update relevant background data.
        :return: Nothing
        """
        super().execute_check()  # Make sure to do the base update before running the check
        w = random.randint(1, 10)
        self.logger.debug(F"Starting wait for {w} seconds")  # Send log messages
        time.sleep(w)
        if w > 8:
            self.status.result = StatusResult.error
            self.status.status_string = F"Slept for {w} seconds"
            self.status.save()
        elif w > 5:
            self.status.result = StatusResult.warning
            self.status.status_string = F"Slept for {w} seconds"
            self.status.save()
        else:
            self.status.result = StatusResult.ok
            self.status.status_string = F"Slept for {w} seconds"
            self.status.save()
