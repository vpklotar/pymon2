from Backend.Blueprint import Blueprint
from Backend.Module import Module
from Backend.StatusObject import StatusObject, StatusResult
import subprocess


class Ping4(Module):
    _warning: float = 100
    _error: float = 500
    _warning_pkg_loss: float = 20
    _error_pkg_loss: float = 20
    _num_pings: int = 3

    def __init__(self, host,  service_model, blueprint: Blueprint):
        super().__init__(host, service_model, blueprint)
        self.logger.debug(F"This is printed using the LOGGER. Coupled to host {self.host}")
        if "warning" in self.arguments:
            self._warning = float(self.arguments['warning'])
        if "error" in self.arguments:
            self._error = float(self.arguments['error'])
        if "warning_pkg_loss" in self.arguments:
            self._warning_pkg_loss = float(self.arguments['warning_pkg_loss'])
        if "error_pkg_loss" in self.arguments:
            self._error_pkg_loss = float(self.arguments['error_pkg_loss'])
        if "num_pings" in self.arguments:
            self._num_pings = int(self.arguments['num_pings'])

    @staticmethod
    def version() -> str:
        return "1.0"

    def execute_check(self):
        super().execute_check()
        cmd = subprocess.Popen(['ping', F"-c {self._num_pings}", self.host.address],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
        lines = cmd.stdout.readlines()
        stat_line1 = lines[-2:][0].decode('utf-8').strip()
        stat_line2 = lines[-2:][1].decode('utf-8').strip()
        percent_loss = float(stat_line1.split(',')[2].split('%')[0].strip())
        if percent_loss != 100:
            avg_time = float(stat_line2.split('=')[1].split('/')[1])
        else:
            avg_time = "-"

        # Perform actual check
        result = StatusResult.ok
        status_line = F"Ping OK, time: {avg_time}, package loss: {percent_loss}%"
        if percent_loss >= self._error_pkg_loss or avg_time >= self._error:
            result = StatusResult.error
            status_line = F"Ping ERROR, time: {avg_time}, package loss: {percent_loss}%"
        elif percent_loss >= self._warning_pkg_loss or avg_time >= self._warning:
            result = StatusResult.warning
            status_line = F"Ping WARNING, time: {avg_time}, package loss: {percent_loss}%"

        self.status.result = result
        self.status.status_string = status_line
        self.status.set_performance_metric('ping_average_time', avg_time)
        self.status.set_performance_metric('ping_percent_loss', percent_loss)
        self.status.save()
