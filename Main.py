#!/usr/bin/python3

import argparse
from PyMon.Backend.Managers.HostManager import HostManager
from PyMon.Backend.Managers.DatabaseManager import DatabaseManager
from PyMon.Backend.Managers.TemplateManager import TemplateManager
from PyMon.Backend.Config import Config
from time import sleep
import GlobalConfig
from PyMon.Backend.REST import REST
import colorlog
import logging


class Main:
    LogLevelArg = 2  # Display all log messages by default

    def __init__(self):
        self.ParseArgs()
        self.InitLogger()
        self.LoadConfig()
        GlobalConfig.SetValue('Config', self.Config)
        self.Logger = GlobalConfig.GetLogger('Main')
        self.Logger.info('Logger initilized')
        # Initialize and connect to all databases
        self.Logger.debug('Starting DatabaseManager')
        self.DatabaseManager = DatabaseManager()
        GlobalConfig.SetValue('DatabaseManager', self.DatabaseManager)
        # Initialize all templates
        self.TemplateManager = TemplateManager()
        GlobalConfig.SetValue('TemplateManager', self.TemplateManager)
        # Initialize all hosts (each host initializes thier own monitors)
        self.Logger.debug('Starting HostManager')
        self.HostManager = HostManager()
        GlobalConfig.SetValue('HostManager', self.HostManager)
        self.Logger.info('Initilization done')
        self.Logger.info('Starting REST Api')
        REST(self).RunRestAPI(False, False)  # Start RestAPI

    def InitLogger(self):
        GlobalConfig.init()
        GlobalConfig.SetValue('LogLevel', self.Arguments.loglevel)
        handler = colorlog.StreamHandler()
        LoggingLevels = {
            'STATUS': 7,
            'PROPERTY': 8
        }
        GlobalConfig.SetValue('LoggingLevels', LoggingLevels)
        logging.addLevelName(LoggingLevels['STATUS'], 'STATUS')
        logging.addLevelName(LoggingLevels['PROPERTY'], 'PROPERTY')
        handler.setFormatter(colorlog.ColoredFormatter(
            '\033[95m[%(asctime)s]%(log_color)s[%(levelname)s]\033[39m\033[1m[%(name)s]\033[0m: %(message)s',
            log_colors={
                'DEBUG':    'cyan',
                'PROPERTY': 'cyan',
                'STATUS':   'cyan',
                'INFO':     'blue',
                'WARNING':  'yellow',
                'ERROR':    'red',
                'CRITICAL': 'red,bg_white',
            }))
        GlobalConfig.SetValue('LogHandler', handler)

        logger = GlobalConfig.GetLogger('Main')
        logger.debug('Arguments parsed')
        logger.info('Logger initiated')

    # Load and initialize config
    def LoadConfig(self):
        self.Config = Config(self.Arguments.config)

    # Parse arguments given via command line
    def ParseArgs(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("-c", "--config", type=str,
                            help="Specify a path to a specific config file",
                            default="config.json")
        parser.add_argument("-l", "--loglevel", type=str, default='STATUS',
                            help="Increase logging (MAX: 5, DEFAULT: 5)")
        self.Arguments = parser.parse_args()


# Run the mail class to handle all of the application
Main()


# Make it not stop when all the subprocess have been launched
while True:
    sleep(10)
