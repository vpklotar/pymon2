import logging


class Signal:
    _arguments: tuple
    _listeners: list
    logger: logging.Logger

    def __init__(self, *args):
        self._arguments = args
        self._listeners = list()
        self.logger = logging.getLogger('Signal')

    def connect(self, callback):
        """Connect a callback function to be called when a signal is emitted"""
        self._listeners.append(callback)

    def emit(self, *args):
        """Emit a signal with the argument types in the order and exact amount as initiated"""
        for listener in self._listeners:
            listener(*args)
