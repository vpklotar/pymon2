from orator import Model
from orator.orm import has_many, belongs_to, belongs_to_many, has_many_through


class ServiceModel(Model):
    __table__ = 'services'
    __fillable__ = ['module', 'check_interval', 'arguments']

    @belongs_to
    def blueprint(self):
        return BlueprintModel

    @has_many
    def history(self):
        return HistoryModel


class ServiceStatusModel(Model):
    __table__ = 'service_status'
    __fillable__ = ['host_id', 'blueprint_id', 'service_id', 'status_result', 'status_string']

    @belongs_to
    def blueprint(self):
        return BlueprintModel

    @belongs_to
    def host(self):
        return HostModel

    @belongs_to
    def service(self):
        return ServiceModel


class HistoryModel(Model):
    __table__ = 'history'

    @belongs_to
    def service(self):
        return ServiceModel


class BlueprintModel(Model):
    __table__ = 'blueprints'
    __fillable__ = ['display_name', 'description']

    @has_many
    def services(self):
        return ServiceModel

    @belongs_to_many(table='hosts_blueprints')
    def hosts(self):
        return HostModel


class HostModel(Model):
    __table__ = 'hosts'
    __fillable__ = ['display_name', 'address']

    @belongs_to_many(table='hosts_blueprints')
    def blueprints(self):
        return BlueprintModel
