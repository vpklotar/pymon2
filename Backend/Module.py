from abc import ABCMeta, abstractmethod

from Backend.Blueprint import Blueprint
from Backend.Host import Host
from Backend.ORM.Base import ServiceModel, ServiceStatusModel
from Backend.Signal import Signal
from Backend.StatusObject import StatusObject, StatusResult
import logging
import json
from datetime import datetime


class Module(metaclass=ABCMeta):
    logger: logging.Logger = None
    _host: Host = None
    _status: StatusObject = None
    _service_model: ServiceModel = None
    _running = False
    _id: int = None
    on_status_update: Signal = None
    _arguments: dict = None
    _do_active_checks: bool = True

    @property
    def arguments(self):
        return self._arguments

    @property
    def module(self):
        return self._service_model.module

    @property
    def history(self):
        return self._service_model.history().get().all()

    @property
    def id(self):
        return self._id

    @property
    def running(self):
        return self._running

    @property
    def status(self):
        return self._status

    @property
    def check_interval(self):
        return self._check_interval

    @property
    def host(self):
        return self._host

    @property
    def blueprint(self):
        return self._blueprint

    @property
    def do_active_checks(self) -> bool:
        """
        Returns if a check should be performed or not
        :return: Bool
        """
        return self._do_active_checks

    def __init__(self, host: Host, service_model, blueprint: Blueprint):
        """
        Initialize the logger for this class according to naming scheme.
        :param host: The host that this instance is associated with.
        :param service_model: The Orator data model for the service
        """
        self._host = host
        self._blueprint = blueprint
        self._service_model = service_model
        self.reload_data_from_database()
        self.logger = logging.getLogger(self.__str__())
        self.perform_sanity_check(service_model)
        self._check_interval = service_model.check_interval
        self.on_status_update = Signal(Module, StatusObject)

    def reload_data_from_database(self):
        """
        Reload data from the database
        :param service_model:
        :return:
        """
        self._id = self._service_model.id
        self._arguments = json.loads(self._service_model.arguments)
        self._check_interval = self._service_model.check_interval
        # Load status
        status_model = ServiceStatusModel.where_raw(F"service_id = {self.id} and host_id = {self.host.id}").first()
        if not status_model:
            status_model = ServiceStatusModel(status_result=StatusResult.unknown.value, status_string="",
                                              host_id=self.host.id, service_id=self.id)
            status_model.save()
        self._status = StatusObject(status_model)
        self._status.on_save.connect(self._status_saved)

    def delete(self):
        """
        Delete this service and all the history/status from the database
        :return: None
        """
        self.status.delete()
        self._service_model.delete()
        self._do_active_checks = False

    def _status_saved(self, status):
        """
        When a new status has been saved/updated signal that it has happened and mark it for execution
        on the next check
        :param status:
        :return:
        """
        self._running = False
        self.on_status_update.emit(self, status)

    @staticmethod
    def perform_sanity_check(service_model) -> None:
        """
        Will do the necessary checks and verify that all needed data is presnt
        :param service_model: The Orator data model for the service
        :return: None
        :raises MissingArgumentException
        """
        pass

    @staticmethod
    @abstractmethod
    def version() -> str:
        """
        Static method to get the service version without creating an instance of it.
        :return: The version string.
        """
        return "0.0.1a"

    @abstractmethod
    def execute_check(self) -> None:
        """
        Will set base values and signal that the module is currently performing a check.
        :return: None
        """
        self._check_start = datetime.now()
        self._running = True

    def __str__(self) -> str:
        """
        :return: A string representation of the instance
        """
        return F"{self.__class__.__name__} ({self.id})"
