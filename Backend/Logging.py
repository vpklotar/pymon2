import logging
from Backend.Config import Config


class Logging:

    @staticmethod
    def init_logging():
        c = Config()
        level_str: str = c.get(path='logging.level', default='warning')
        # Begin by settings logging level to INFO in order to print the logging level to stdout
        logging_level: int = logging.INFO
        logging.basicConfig(level=logging_level)
        if level_str.lower() == 'error':
            logging.info("Setting logging level to: ERROR")
            logging.info()
            logging_level = logging.ERROR
        elif level_str.lower() == 'warning':
            logging.info("Setting logging level to: WARNING")
            logging_level = logging.WARNING
        elif level_str.lower() == 'info':
            logging.info("Setting logging level to: INFO")
            logging_level = logging.INFO
        elif level_str.lower() == 'debug':
            logging.info("Setting logging level to: DEBUG")
            logging_level = logging.DEBUG
        else:
            logging.error('ERROR: Failed to interpret logging level. Valid values: error|warning|info|debug.')
            logging.info("Setting logging level to: INFO")

        # Update logging level according to config value
        logging.basicConfig(level=logging_level)
        logging.getLogger().setLevel(level=logging_level)
