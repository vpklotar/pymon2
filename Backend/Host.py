import logging
from typing import List
from Backend.ORM.Base import HostModel
from Backend.StatusObject import StatusResult


class Host:
    _address: str = None
    _display_name: str = None
    _state: StatusResult = None
    _checks: list = None
    _id: int = None
    _host_model: HostModel = None
    _blueprints: List = None
    _services: List = None

    @property
    def id(self):
        return self._id

    @property
    def address(self) -> str:
        return self._address

    @property
    def display_name(self) -> str:
        return self._display_name

    @display_name.setter
    def display_name(self, new_display_name: str):
        old_display_name = self._display_name
        self._host_model.display_name = new_display_name
        self._host_model.save()
        self._display_name = new_display_name
        # Update logger to match new name
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info(F"Host '{old_display_name}' changed name to '{new_display_name}'.")

    @property
    def state(self) -> StatusResult:
        return self._state

    @state.setter
    def state(self, new_state: StatusResult):
        self._state = new_state

    @property
    def checks(self) -> list:
        return self._checks

    @property
    def services(self) -> List:
        return self._services

    @property
    def blueprints(self):
        return self._blueprints

    def attach_to_blueprint(self, blueprint):
        """
        Attach a blueprint to this host. This is only used to do lookups
        :param blueprint: The blueprint to add
        :return:
        """
        self._blueprints.append(blueprint)

    def detach_from_blueprint(self, blueprint):
        """
        Remove a reverse lookup blueprint connection
        :param blueprint: The blueprint to remove
        :return:
        """
        self._blueprints.remove(blueprint)

    def attach_to_service(self, service):
        """
        Attach a service to this host. This is only used to do lookups
        :param service: The blueprint to add
        :return:
        """
        self._services.append(service)

    def detach_from_service(self, service):
        """
        Remove a service lookup blueprint connection
        :param service: The blueprint to remove
        :return:
        """
        self._services.remove(service)

    def add_blueprint(self, blueprint):
        """
        Apply a new blueprint to this host
        :param blueprint: The blueprint to apply
        :return:
        """
        self._host_model.blueprints().save(blueprint.raw_blueprint)
        blueprint.attach_host(self)

    def remove_blueprint(self, blueprint):
        """
        Apply a new blueprint to this host
        :param blueprint: The blueprint to apply
        :return:
        """
        self._host_model.blueprints().remove(blueprint.raw_blueprint)
        blueprint.detach_host(self)

    @property
    def host_model(self):
        return self._host_model

    def __init__(self, host_model):
        self.logger = logging.getLogger(self.__class__.__name__)
        self._set_data_according_to_model(host_model)
        self._blueprints = list()
        self._services = list()
        self._state = StatusResult.unknown

    def _set_data_according_to_model(self, model):
        """Populate the host according to the data retrieved from the database"""
        self._host_model = model
        self._id = model.id
        self._address = model.address
        self._display_name = model.display_name

    def __str__(self):
        return F"{self.display_name} ({self.id})"

