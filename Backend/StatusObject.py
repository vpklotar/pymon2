from datetime import datetime
from enum import IntEnum

from Backend.ORM.Base import ServiceStatusModel
from Backend.Signal import Signal


class StatusResult(IntEnum):
    ok = 0
    warning = 1
    error = 3
    unknown = 4
    timeout = 5


class StatusObject:
    _performance_data = None
    _deleted = False
    _raw_service_status_model: ServiceStatusModel = None
    on_save: Signal = None

    def __init__(self, service_status_model: ServiceStatusModel):
        self._raw_service_status_model = service_status_model
        self._performance_data = {}
        self.on_save = Signal(StatusObject)

    @property
    def status_string(self) -> str:
        return self._raw_service_status_model.status_string

    @status_string.setter
    def status_string(self, new_value: str):
        self._raw_service_status_model.status_string = new_value

    @property
    def performance_data(self):
        return self._performance_data

    @performance_data.setter
    def performance_data(self, new_data: dict):
        self._performance_data = new_data

    def set_performance_metric(self, key: str, value):
        self._performance_data[key] = value
        return self

    @property
    def result(self) -> StatusResult:
        return StatusResult(self._raw_service_status_model.status_result)

    @result.setter
    def result(self, new_result: StatusResult):
        self._raw_service_status_model.status_result = new_result.value

    @property
    def updated_at(self):
        return self._raw_service_status_model.updated_at

    def save(self):
        """
        Save all updates to the database
        :return:
        """
        if not self._deleted:
            self._raw_service_status_model.save()
            self.on_save.emit(self)

    def delete(self):
        """
        Delete the entry and the database and make it so that this object cannot be recreated (a new instance is needed)
        :return:
        """
        self._deleted = True
        self._raw_service_status_model.delete()

    def __str__(self):
        return F"StatusObject ({self.result})"
