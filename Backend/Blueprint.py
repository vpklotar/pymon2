from typing import List

from Backend.Host import Host
from Backend.ORM.Base import BlueprintModel
from Backend.Signal import Signal
from components.HostManager import HostManager


class Blueprint:
    _blueprint_model: BlueprintModel = None
    _hosts: List[Host] = None
    _services: List = None
    on_host_added: Signal = None

    @property
    def raw_services(self):
        """
        Get the raw service models from the database
        :return:
        """
        return self._blueprint_model.services.all()

    @property
    def services(self) -> List:
        """
        Return all loaded Service objects attached to this blueprint
        :return:
        """
        return self._services

    def add_service(self, service):
        """
        Add a Service object to this blueprint
        :param service: The Service object to add
        :return:
        """
        self._services.append(service)

    def remove_service(self, service):
        """
        Remove a Service object from this blueprint
        :param service: The Service object to remove
        :return:
        """
        self._services.remove(service)
        service.delete()

    @property
    def raw_hosts(self):
        """
        Get the raw host models from the database
        :return:
        """
        return self._blueprint_model.hosts.all()

    @property
    def hosts(self) -> List[Host]:
        """
        Return all loaded Host objects attached to this blueprint
        :return:
        """
        return self._hosts

    def attach_host(self, host: Host):
        """
        Add a host object to this blueprint
        :param host: The Service object to add
        :return:
        """
        host.attach_to_blueprint(self)
        self._hosts.append(host)
        self.on_host_added.emit(self, host)

    def detach_host(self, host: Host):
        """
        Remove a Service object from this blueprint
        :param host: The Service object to remove
        :return:
        """
        host.detach_from_blueprint(self)
        self._hosts.remove(host)

    @property
    def id(self) -> id:
        """
        The ID of this blueprint
        :return:
        """
        return self._blueprint_model.id

    @property
    def display_name(self) -> str:
        """
        The configured display name of this blueprint
        :return:
        """
        return self._blueprint_model.display_name

    @display_name.setter
    def display_name(self, new_name: str):
        """
        Set a new display_name for this blueprint
        :param new_name: The new name
        :return:
        """
        self._blueprint_model.display_name = new_name

    @property
    def description(self) -> str:
        """
        The configured description of this blueprint
        :return: Blueprint description
        """
        return self._blueprint_model.description

    @description.setter
    def description(self, new_description):
        """
        Set a new description for this blueprint
        :param new_description: The new description
        :return: Blueprint description
        """
        self._blueprint_model.description = new_description

    @property
    def raw_blueprint(self):
        return self._blueprint_model

    def save(self):
        """
        Save any changes made to the blueprint
        :return:
        """
        self._blueprint_model.save()

    def delete(self):
        """
        Delete this blueprint and remove all connections to services/hosts
        :return: None
        """
        self._blueprint_model.delete()

    def __init__(self, blueprint_model: BlueprintModel):
        self._blueprint_model = blueprint_model
        self._hosts = list()
        self._services = list()
        self.on_host_added = Signal(Blueprint, Host)
        HostManager().on_host_added.connect(self._host_added)

    def _host_added(self, host: Host):
        if host not in self._hosts:
            for raw_host in self.raw_hosts:
                if raw_host.id == host.id:
                    self.attach_host(host)
                    return None
