import json
from Backend.Singleton import Singleton
import logging


class Config(Singleton):
    _config: dict = None

    def __init__(self):
        super(Config, self).__init__()
        with open('config.json') as f:
            self._config = json.loads(f.read())

    def get(self, path: str, default=None):
        parts = path.split('.')
        segment = self._config
        for part in parts[:-1]:
            if part in segment:
                segment = segment[part]
            else:
                logging.getLogger('Config').error(F"Requested value {path} does not exist. Returning default.")
                return default
        if parts[len(parts)-1] in segment:
            return segment[parts[len(parts)-1]]
        else:
            logging.getLogger('Config').error(F"Requested value {path} does not exist. Returning default.")
            return default

    def set(self, path, value):
        parts = path.split('.')
        segment = self._config
        for part in parts[:-1]:
            if part not in segment:
                segment[part] = {}
            segment = segment[part]

        segment[parts[len(parts) - 1]] = value

    def delete(self, path):
        parts = path.split('.')
        segment = self._config
        for part in parts[:-1]:
            if part not in segment:
                segment[part] = {}
            segment = segment[part]

        if parts[len(parts) - 1] in segment:
            segment.pop(parts[len(parts) - 1])

    def exist(self, path) -> bool:
        parts = path.split('.')
        segment = self._config
        for part in parts[:-1]:
            if part not in segment:
                segment[part] = {}
            segment = segment[part]

        return parts[len(parts) - 1] in segment

    def save(self):
        """Save config to config.json"""
        with open('config.json', 'w') as f:
            f.write(json.dumps(self._config, indent=2))
