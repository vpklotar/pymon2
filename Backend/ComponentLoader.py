import sys
import traceback

from Backend.Singleton import Singleton
from Backend.Config import Config
import os
import logging


class ComponentLoader(Singleton):
    _components: dict = {}

    def __init__(self):
        self.logger = logging.getLogger("ComponentLoader")

    def _import_component(self, component_name: str):
        """Import the specified python module"""
        if component_name not in self._components:
            components_path = Config().get(path='paths.components_directory', default='components')
            component_path = os.path.join(components_path, F"{component_name}.py")
            if os.path.isfile(component_path):
                mod = __import__(F"{components_path}.{component_name}", fromlist=[component_name])
                self._components[component_name] = mod
            else:
                raise FileNotFoundError(F"Component {component_name} not found. Make sure the file '{component_path}' exists.")
        else:
            self.logger.debug(F"Trying to load module {component_name} when it is already loaded.")

    def _load_component(self, component_name: str) -> None:
        """Load and initialize a component"""
        try:
            self._import_component(component_name)
            module = self._components[component_name]  # Load the module
            cls = getattr(module, component_name)  # Get the class from the loaded module
            self.logger.debug(F"About to instantiate module '{component_name}' "
                                                     F"version {cls.version()}")
            instance = cls()
            instance.connect_signals()
            instance.run()
        except Exception as e:
            self.logger.error(F"Failed to create instance of component {component_name}. Error:")
            tb = sys.exc_info()[2]
            self.logger.error(e)
            traceback.print_tb(tb)

    def load_components_from_config(self):
        components_to_load = Config().get(path="components", default=[])
        for component in components_to_load:
            self._load_component(component)
