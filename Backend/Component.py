from abc import abstractmethod


class Component:
    """
    This is an abstract class that should be used as a template for creating new components
    """

    @abstractmethod
    def connect_signals(self):
        """Connect to all signals this component is meant to listen to"""
        pass

    @abstractmethod
    def run(self):
        """This method is called when the component is started"""
        pass

    @staticmethod
    @abstractmethod
    def version() -> str:
        """Return the version of the component"""
        return "0.0.1a"
